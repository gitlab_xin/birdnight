---
title: PHP扩展安装
date: 2020-03-02
categories:
 - PHP
tags:
 - PHP
---

## PHP扩展安装

### 下载并解压
```shell script

#下载扩展包
wget https://pecl.php.net/get/redis-5.3.3.tgz

#下载完后解压并进入相应目录
tar -xzvf redis-5.3.4.tgz

#进入文件夹
cd redis-5.2.0

#用phpize生成configure配置文件
#系统不同，phpize的路径会不一样，在此之前可以输入find / -name phpize，查询下自己的phpize在哪里
/usr/bin/phpize

# 同样，php-config的路径也会因人而异，find / -name php-config 下，来see see自己的路径是多少
./configure --with-php-config=/usr/local/php7/bin/php-config 

#系统里面缺少gcc模块
yum install gcc

#继续编译
make && make install

```

### PHP关联Redis

```shell script

#查看文件路径
find / -name php.ini 

#编辑文件
vim php.ini

#添加文件内容
extension_dir = "/usr/lib64/php/modules/"
extension=redis.so

#/etc/php.d/中也要软连接redis.so，并且要创建一个redis.ini文件。
vim /etc/php.d/redis.ini

#文件内容
extension=redis.so

```

### 安装redis

```shell script

#安装redis  
yum install redis

# 重启php-fpm
systemctl restart php-fpm

#查看是否安装成功
php -m

```