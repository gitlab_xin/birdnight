---
title: Charles使用 
date: 2021-03-03  
categories:
 - Tool
tags:
 - Tool
---

## 断点测试

* 设置Charless断点


> Proxy -> Breakpoint settings -> Add    
> 输入 scheme(选择Get 或者Post)、Protocal(选择 http 或者 https ) 、Host (IP地址)、Port(端口号),Path -> 勾选Request 或者 Response -> OK

  
* 指定URL进行Charless断点
 

> 选中需要断点调试的请求, 右键 -> Breakpoints

> 点击Edit Request -> 选中下方的JSON Text -> 修改参数 -> 点击 Execute.
> 然后会出现 Edit Response 选项, 同Edit Request 操作步骤即可


<img :src="$withBase('/img/WechatIMG779.png')" width="70%">

<img :src="$withBase('/img/WechatIMG780.png')" width="70%">
 

## 模拟慢速网络

> 在 Charles 的菜单上，选择 “Proxy”–>“Throttle Setting” 项，

* throttle preset 流量预设
* bandwidth(kbps):带宽
* utilisation(%)：利用率
* round-trip latency(ms)：来回延迟
* MTU(bytes)：网络最大传输单元
* reliability（%）：可靠性
* stability（%）：稳定性
* unstable quality range（%）：质量不稳定范围

<img :src="$withBase('/img/WechatIMG782.png')" width="50%">

## 压测

> 在想打压的网络请求上（POST 或 GET 请求均可）右击，然后选择 「Repeat Advanced」菜单项

选择打压的并发线程数以及打压次数，确定之后，即可开始打压。

<img :src="$withBase('/img/WechatIMG783.png')" width="70%">



