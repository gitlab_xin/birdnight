---
title: Charles安装及代理  
date: 2021-03-02  
categories:
 - Tool
tags:
 - Tool
---

## Charles破解安装及使用

### 安装破解

* 百度网盘：链接:https://pan.baidu.com/s/1EfTNrRzWE1NgrUfnfF8ORA  密码:7aah
* 将 Charles.app 拖至 应用程序 文件夹
* 在菜单——help菜单下—register Charles下，输入下列序列码
    - Registered Name: https://zhile.io
    - License Key: 48891cf209c6d32bf4
    
### pc端安装Charles证书

*  安装Charles证书

<img :src="$withBase('/img/1196947606-5e4e8de439545_articlex.png')" width="80%">

* 钥匙串搜索证书。（启动台->其他->钥匙串），点击信任，并设置始终信任

<img :src="$withBase('/img/217749289-5e4e8f2d89426_articlex.png')" width="50%">

* 用Safari浏览器打开https://chls.pro/ssl地扯，双击安装SSL证书

* 最后一步，打开Charles，上方菜单栏 ->Proxy —>SSL Proxy Settings —>Add
    - Host：填*表示所有网站都抓 
    - Port：443 
    
<img :src="$withBase('/img/WechatIMG775.png')" width="50%">
    
## 在Mac上用Charles给iPhone抓包

### Charles打开代理

* 打开Charles,点击Proxy -> 点击Proxy Setting，Port填8888，勾上Enable transparent HTTP proxying
* 找到当前使用的Mac的地址，可以点Charles->Help->Local IP Addres。也可以在terminal中输入ifconfig，en0中inet后面接的就是本机IP
  
### iPhone设置代理

* 打开iPhone，连接wifi，确保手机和Mac连的是同一个wifi。
* 点击wifi名后面的蓝色感叹号，在最下面找到“HTTP 代理”，点击进入
* 选择“手动”，服务器填上面找的IP地址，端口填8888。
* 退出设置，随便连上任意的网络，Charles会弹出请求连接的确认菜单。选择“Allow”
* 在手机的浏览器中，输入chls.pro/ssl，会弹出安装证书的请求  
<img :src="$withBase('/img/20191113111138996.jpg')" width="40%">
<img :src="$withBase('/img/20191113111414471.png')" width="40%">
* 进去之后直接安装即可IOS10.3 以上去 设置->通用->关于本机->证书信任设置 设置信任证书

此时，已经能截取到HTTP/HTTPS的网络请求了，在Charles中选择Structure视图，点击想查看的网络请求，点击content，就能查看到json数据了。



