---
title: Http、Https基本概念
date: 2020-10-23
categories:
 - Network
tags:
 - HTTP
---


# HTTP
## 一、基本概念
> 1、HTTP（HyperText Transfer Protocol：超文本传输协议）应用层协议。  
> 2、TCP协议80端口。  
> 3、 明文传输、未加密。

## 二、存在问题
> 1、请求信息明文传输，容易被窃听截取。  
> 2、数据的完整性未校验，容易被篡改。  
> 3、没有验证对方身份，存在冒充危险。

# HTTPS(HTTP+SSL/TLS)
## 一、基本概念
>1、（Hypertext Transfer Protocol Secure：超文本传输安全协议）传输层协议。   
>2、TCP协议443端口。  
>3、密文加密，SSL/TLS加密，
 
## 二 、解决HTTP对应问题
>1、采用对称加密数据报文，非对称加密密钥。   
>2、采用数字签名。    
>3、 采用数字证书。

## 三、TCP三次握手、四次挥手

<img :src="$withBase('/img/WechatIMG693.png')">

> 为防止客户端在第一次发生连接请求超时，而导致服务端发出一个确认报文段。   
> 为避免服务端的资源浪费。

<img :src="$withBase('/img/WechatIMG694.png')">

# HTTPS工作原理

<img :src="$withBase('/img/BEJFYD.png')">

>1、client----->server，（随机值1、支持的加密算法）。   
>2、server---->client，（随机值2，匹配好的协商算法）。   
>3、server---->client，（数字证书）。   
>4、client，（解析证书，无误生成一个随机值3(预主密钥)）。   
>5、client----->server，（将三个随机值组装成回话密钥，通过证书的公钥进行非对称加密）。   
>6、server，（通过私钥进行解密，得到会话密钥）。    
>7、client----->server，（通过会话密钥对一条信息进行对称加密，验证服务端能否正确接受）。   
>8、server----->client，（通过会话密钥回传一条给客户端，正常接受则连接建立，反之亦然）。


