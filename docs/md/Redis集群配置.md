---
title: Redis集群配置（高可用、高并发）
date: 2021-02-04
categories:
 - Redis
tags:
 - Redis
---


> 高可用：24小时对外提供服务  
> 高并发：同一时间段能处理的请求数

## 中心化和去中心化

### 中心化

> 所有节点都有一个主节点

缺点：

* 中心挂了，服务就挂了
* 中心处理数据的能力有限，不能把节点性能发挥到最大

优点：

* 读写分离

<img :src="$withBase('/img/WechatIMG751.png')" width="50%">

### 去中心化

<img :src="$withBase('/img/WechatIMG752.png')" width="50%">

优点：

* 每台节点可自主选择写或读


## Redis集群简介


Redis集群是一个提供在多个Redis节点之间共享数据的程序集

Redis集群并不支持多个keys的命令，因为这需要在不同节点间移动数据，从而达不到像Redis那样的性能，在高负载的情况下可能导致不可预估的错误

Redis集群通过分区来提供一定程度上的可用性，在实际环境当中当某个节点宕机或者不可达的情况下继续处理命令

优势：

* 自动分割数据到不同节点上
* 整个集群的部分节点失败或者不可达的情况下能继续处理命令


## Redis集群的数据分片


Redis集群没有使用一致性hash，而是引入了哈希槽的概念

Redis集群中有16384个哈希槽，每个key通过CRC16校验后对16384取模来决定放置在哪个槽，集群的每一个节点负责一个槽

举个例子，比如当前集群中有3个节点，那么：

* 节点A包含0到5500号哈希槽
* 节点B包含5501到11000号哈希槽
* 节点C包含11001到16384号哈希槽

这种结构很容易添加或者删除节点

* 比如我想添加节点D，我需要从节点A、B、C中部分槽挪到节点D。
* 如果我想移除节点A，需要将节点A中的槽移到B和C节点上，然后将没有任何槽的A节点移除即可
* 由于从一个节点将哈希槽移动到另一个节点并不会停止服务，所以无论添加删除移动或者改变某个节点的哈希槽的数量都不会造成集群不可用的状态


## Redis集群的主从复制模型


为了使在部分节点失败或者大部分节点无法通信的情况下集群仍然可用，所以集群使用了主从复制模型，每个节点都会有N+1个复制品

举个例子，有A、B、C三个节点的集群：

* 在没有主从复制模型的情况下，如果节点B失败了，那么整个集群就会缺失5501-11000这个范围的槽而不可用
* 如果我们为每个节点创建一个从节点A1、B2、C2，那么整个集群便有三个master和三个slave节点组成
* 这样在节点B失败后，集群便会选举B1为新的主节点继续服务，整个集群便不会因为槽找不到而不可用了


## Redis一致性保证


Redis并不能保证数据的强一致性，这意味在实际中集群在特定条件下可能会丢失写操作

原因：

* 客户端向主节点B写入一条命令
* 节点B向客户端回复命令状态
* 主节点将写操作复制给他的从节点B1、B2

主节点对命令的复制发生在返回命令回复之后（因为如果每次处理命令请求都需要等待复制操作完成的话，主节点处理命令的请求的速度将极大的降低）

## 集群搭建

### 本机端口准备

|  服务器地址  | 服务器角色  |  
|  ----  | ----  |
|  127.0.0.1:7000  | master  | 
|  127.0.0.1:7001  | master  | 
|  127.0.0.1:7002  | master  | 
|  127.0.0.1:7003  | slave  | 
|  127.0.0.1:7004  | slave  | 
|  127.0.0.1:7005  | slave  | 

### redis.conf配置

* redis.conf （某一节点配置）
```$xslt

# 允许所有ip访问（测试）
bind 0.0.0.0

# 端口
port 7000

# 开启集群
cluster-enabled yes

```

### docker-compose.yml

```$xslt

version: "2"
services:
    redis-cluster-7000:
    image: redis:6.2-rc1-alpine3.12
    container_name: "redis-cluster-7000"
    network_mode: host
    restart: always
    environment:
      - REDIS_PORT=7000
    volumes:
      - /root/docker/redis/redis-cluster/redis-7000.conf:/etc/redis-7000.conf
    command:
      /bin/sh -c "redis-server /etc/redis-7000.conf"
    redis-cluster-7001:
    image: redis:6.2-rc1-alpine3.12
    container_name: "redis-cluster-7001"
    restart: always
    environment:
      - REDIS_PORT=7001
    network_mode: "host"
    volumes:
      - /root/docker/redis/redis-cluster/redis-7001.conf:/etc/redis-7001.conf
    command:
      /bin/sh -c "redis-server /etc/redis-7001.conf"
    redis-cluster-7002:
    image: redis:6.2-rc1-alpine3.12
    container_name: "redis-cluster-7002"
    restart: always
    environment:
      - REDIS_PORT=7002
    network_mode: "host"
    volumes:
      - /root/docker/redis/redis-cluster/redis-7002.conf:/etc/redis-7002.conf
    command:
      /bin/sh -c "redis-server /etc/redis-7002.conf"
    redis-cluster-7003:
    image: redis:6.2-rc1-alpine3.12
    container_name: "redis-cluster-7003"
    restart: always
    environment:
      - REDIS_PORT=7003
    network_mode: "host"
    volumes:
      - /root/docker/redis/redis-cluster/redis-7003.conf:/etc/redis-7003.conf
    command:
      /bin/sh -c "redis-server /etc/redis-7003.conf"
    redis-cluster-7004:
    image: redis:6.2-rc1-alpine3.12
    container_name: "redis-cluster-7004"
    restart: always
    environment:
      - REDIS_PORT=7004
    network_mode: "host"
    volumes:
      - /root/docker/redis/redis-cluster/redis-7004.conf:/etc/redis-7004.conf
    command:
      /bin/sh -c "redis-server /etc/redis-7004.conf"
    redis-cluster-7005:
    image: redis:6.2-rc1-alpine3.12
    container_name: "redis-cluster-7005"
    restart: always
    environment:
      - REDIS_PORT=7005
    network_mode: "host"
    volumes:
      - /root/docker/redis/redis-cluster/redis-7005.conf:/etc/redis-7005.conf
    command:
      /bin/sh -c "redis-server /etc/redis-7005.conf"

```

### 启动6个redis实例（切换到docker-composer.yml文件目录）

```$xslt
docker-compose up -d
```

```$xslt

CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS               NAMES
ceee10020852        redis:6.2-rc1-alpine3.12   "docker-entrypoint..."   25 minutes ago      Up 13 minutes                           redis-cluster-7004
52bcc6e16118        redis:6.2-rc1-alpine3.12   "docker-entrypoint..."   25 minutes ago      Up 13 minutes                           redis-cluster-7003
b97eea705f1a        redis:6.2-rc1-alpine3.12   "docker-entrypoint..."   25 minutes ago      Up 13 minutes                           redis-cluster-7001
9c8aee6cf612        redis:6.2-rc1-alpine3.12   "docker-entrypoint..."   25 minutes ago      Up 13 minutes                           redis-cluster-7002
8c85e3be9c67        redis:6.2-rc1-alpine3.12   "docker-entrypoint..."   25 minutes ago      Up 13 minutes                           redis-cluster-7005
37041c04d110        redis:6.2-rc1-alpine3.12   "docker-entrypoint..."   25 minutes ago      Up 13 minutes                           redis-cluster-7000


```

* 这个时候只是启动了6个单实例的redis，此时还没有进行集群操作

### 集群操作

```$xslt

docker run -it --net host inem0o/redis-trib create --replicas 1 127.0.0.1:7000 127.0.0.1:7001 127.0.0.1:7002 127.0.0.1:7003 127.0.0.1:7004 127.0.0.1:7005

```

```$xslt

>>> Creating cluster
>>> Performing hash slots allocation on 6 nodes...
Using 3 masters:
127.0.0.1:7000
127.0.0.1:7001
127.0.0.1:7002
Adding replica 127.0.0.1:7003 to 127.0.0.1:7000
Adding replica 127.0.0.1:7004 to 127.0.0.1:7001
Adding replica 127.0.0.1:7005 to 127.0.0.1:7002
M: 9b4909f525d2a191ef65d6b4f80dd0a263564550 127.0.0.1:7000
   slots:0-5460 (5461 slots) master
M: 08f04452d578c45de9e3a9846488685c93f4d7ff 127.0.0.1:7001
   slots:5461-10922 (5462 slots) master
M: 382ae9a584ede4b63b5d87aa0c05496a4ac6e065 127.0.0.1:7002
   slots:10923-16383 (5461 slots) master
S: d90444e9761821e8029766aa762e1372efaa1b2f 127.0.0.1:7003
   replicates 9b4909f525d2a191ef65d6b4f80dd0a263564550
S: 85494fb4c338ac319f043106acee972a602db75e 127.0.0.1:7004
   replicates 08f04452d578c45de9e3a9846488685c93f4d7ff
S: 85deab2ea6a530a45ed9c5e97dae11fa23abbe30 127.0.0.1:7005
   replicates 382ae9a584ede4b63b5d87aa0c05496a4ac6e065
Can I set the above configuration? (type 'yes' to accept): yes
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join.
>>> Performing Cluster Check (using node 127.0.0.1:7000)
M: 9b4909f525d2a191ef65d6b4f80dd0a263564550 127.0.0.1:7000
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: 85deab2ea6a530a45ed9c5e97dae11fa23abbe30 127.0.0.1:7005@17005
   slots: (0 slots) slave
   replicates 382ae9a584ede4b63b5d87aa0c05496a4ac6e065
S: d90444e9761821e8029766aa762e1372efaa1b2f 127.0.0.1:7003@17003
   slots: (0 slots) slave
   replicates 9b4909f525d2a191ef65d6b4f80dd0a263564550
M: 08f04452d578c45de9e3a9846488685c93f4d7ff 127.0.0.1:7001@17001
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
S: 85494fb4c338ac319f043106acee972a602db75e 127.0.0.1:7004@17004
   slots: (0 slots) slave
   replicates 08f04452d578c45de9e3a9846488685c93f4d7ff
M: 382ae9a584ede4b63b5d87aa0c05496a4ac6e065 127.0.0.1:7002@17002
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.

```

* 集群已启动完毕

## 集群使用

### 连接其中一个redis-cluster容器

```$xslt

# ceee10020852为我的容器地址，可自行更换
docker exec -it ceee10020852 /bin/sh

# 查看此节点集群信息
redis-cli -p 7004 cluster nodes

#在下面列出的六行行信息中， 从左到右的各个域分别是： 节点 ID ， IP 地址和端口号， 标志（flag）， 最后发送 PING 的时间， 最后接收 PONG 的时间， 连接状态， 节点负责处理的槽
9b4909f525d2a191ef65d6b4f80dd0a263564550 127.0.0.1:7000@17000 master - 0 1612671767488 1 connected 0-5460
382ae9a584ede4b63b5d87aa0c05496a4ac6e065 127.0.0.1:7002@17002 master - 0 1612671765000 3 connected 10923-16383
08f04452d578c45de9e3a9846488685c93f4d7ff 127.0.0.1:7001@17001 master - 0 1612671767000 2 connected 5461-10922
d90444e9761821e8029766aa762e1372efaa1b2f 127.0.0.1:7003@17003 slave 9b4909f525d2a191ef65d6b4f80dd0a263564550 0 1612671767000 1 connected
85deab2ea6a530a45ed9c5e97dae11fa23abbe30 127.0.0.1:7005@17005 slave 382ae9a584ede4b63b5d87aa0c05496a4ac6e065 0 1612671768490 3 connected
85494fb4c338ac319f043106acee972a602db75e 127.0.0.1:7004@17004 myself,slave 08f04452d578c45de9e3a9846488685c93f4d7ff 0 1612671768000 2 connected

# -c 以集群方式启动
# -p 指定端口
redis-cli -c -p 7004

```

### 写操作展示

```$xslt

# 此时无任何数据，且位于7004端口的实例当中
127.0.0.1:7004> keys *
(empty array)

#进行写操作后，发现已经连接到7002的端口当中
127.0.0.1:7004> set k1 v1
-> Redirected to slot [12706] located at 127.0.0.1:7002
OK
127.0.0.1:7002> keys *
1) "k1"

```
















