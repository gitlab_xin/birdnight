---
title: Redis.conf配置
date: 2021-01-10
categories:
 - Redis
tags:
 - Redis
---

# redis.conf配置

## network - 网络

### bind
* bind 127.0.0.1
* 绑定的主机地址.，如果没有绑定，所有接口都会监听到来的连接

### port

* port 6379
* 指定Redis监听的端口，默认端口为6379， 如果指定0端口，表示Redis不监听TCP连接

### timeout

* timeout 300
* 当客户端闲置多少秒后关闭功能，如果指定为0，表示关闭该功能

### protected-mode

* protected-mode yes

### tcp-backlog

* tcp-backlog
* 设置tcp的backlog，backlog其实是一个连接队列，backlog队列总和 = 未完成三次握手队列 + 已经完成三次握手队列。
* 高并发的情况下，你需要一个高backlog值来避免客户端连接问题，注意linux内核会将这个值减小到/proc/sys/net/core/somaxconn的值，所以需要确认增大somaxconn和tcp_max_syn_backlog两个值

### tcp-keepalive

* tcp-keepalive 300
* 单位是秒，表示将周期性的使用SO_KEEPALIVE检测客户端是否还处于健康状态，避免服务器一直堵塞

## GENERAL - 常规

### daemonize

* daemonize no
* 守护模式，配置为yes时以守护模式启动，这时redis instance会将进程号pid写入默认文件/var/run/redis.pid

### supervised

* supervised no
* 可以通过upstart和systemd管理redis守护进程，这个参数是和具体的操作系统相关的

### pidfile

* pidfile /var/run/redis_6379.pid
* 配置pid文件路径

### loglevel

* loglevel notice
* debug - 调试（很多信息，对于开发/测试很有用）
* verbose - 详细（很多有用的信息，但不会像调试级别那样混乱）
* notice - 通知（适用于生产环境）
* warning - 警告（只记录警告或错误信息）

### logfile

* logfile ""
* 日志记录路径

### syslog-enabled 

* syslog-enabled no
* 是否将日志记录到系统当中

### syslog-ident

* syslog-ident redis
* 设置日志的前缀

### syslog-facility 

* syslog-facility local0
* 

### databases

* databases 16
* 设置数据库的数量，默认数据库为0

### always-show-logo

* always-show-logo yes
* 是否一直显示日志



## SNAPSHOTTING

### save

* save 900 1
* save 300 10
* save 60 10000
* 指定在规定时间内，有多少次更新操作，就将数据同步到数据库文件，可以多个配合

### stop-writes-on-bgsave-error

* stop-writes-on-bgsave-error yes
* 默认情况下，如果最后一次的后台保存失败，redis将停止接受写操作，这样以一种强硬的方式让用户知道数据不能正常的持久化到硬盘。

### rdbcompression

* rdbcompression yes
* 指定存储至本地数据库时是否进行压缩数据，Redis采用LZF压缩
* 如果为了节省CPU时间，可以关闭此选项，但会导致数据库文件变的巨大

### rdbchecksum

* rdbchecksum yes
* 是否进行CRC64校验rdb文件，会有一定的性能损失

### dbfilename

* dbfilename dump.rdb
* 指定本地数据库的rdb文件名

### rdb-del-sync-files

* rdb-del-sync-files no

### dir

* dir ./
* 指定本地数据库存放目录

## REPLICATION

### replicaof（旧版本是slaveof）

* replicaof < masterip> < masterport>
* 设置当本机为 slave 服务时，设置 master 服务的 IP 地址及端口
* 在 Redis 启动时，它会自动从 master 进行数据同步

### masterauth

* masterauth  < master-password>
* 当master服务设置了密码保护时，slav 服务连接 master 的密码

### masteruser

* masteruser < username>

### replica-serve-stale-data

* replica-serve-stale-data yes

### replica-read-only

* replica-read-only yes

### repl-diskless-sync

* repl-diskless-sync no

### repl-diskless-sync-delay

* repl-diskless-sync-delay 5

### repl-diskless-load

* repl-diskless-load disabled

### repl-disable-tcp-nodelay

* repl-disable-tcp-nodelay no

### replica-priority

* replica-priority 100

## SECURITY - 安全

### acllog-max-len

* acllog-max-len 128

### requirepass

* requirepass foobared
* 设置 Redis 连接密码，默认关闭

## CLIENTS

### maxclients

* maxclients 10000
* 设置同一时间最大客户端连接数，默认无限制
* 如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis 会关闭新的连接并向客户端返回 max number of clients reached 错误信息

## MEMORY MANAGEMENT

### maxmemory

* maxmemory < bytes>
* 指定 Redis 最大内存限制，Redis 在启动时会把数据加载到内存中，达到最大内存后，Redis 会先尝试清除已到期或即将到期的 Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis 新的 vm 机制，会把 Key 存放内存，Value 会存放在 swap 区

### maxmemory-policy

* maxmemory-policy noeviction
* volatile-lru：使用LRU算法移除key，只对设置了过期时间的键
* allkeys-lru：使用LRU算法移除key
* volatile-random：在过期集合中移除随机的key，只对设置了过期时间的键
* allkeys-random：移除随机的key
* volatile-ttl：移除哪些ttl值最小的key，即那些最近要过期的key
* noeviction：不进行移除。只针对写操作，只是返回错误信息

### maxmemory-samples

* maxmemory-samples 5

### replica-ignore-maxmemory 

* replica-ignore-maxmemory yes

### active-expire-effort

* active-expire-effort 1

## LAZY FREEING

### lazyfree-lazy-eviction

* lazyfree-lazy-eviction no

### lazyfree-lazy-expire

*  lazyfree-lazy-expire no


### lazyfree-lazy-server-del

* lazyfree-lazy-server-del no

### replica-lazy-flush

* replica-lazy-flush no

### lazyfree-lazy-user-del

* lazyfree-lazy-user-del no

## THREADED I/O

### io-threads 

* io-threads 4

### io-threads-do-reads

* io-threads-do-reads no

## KERNEL OOM CONTROL

### oom-score-adj

* oom-score-adj no

### oom-score-adj-values

* oom-score-adj-values  0 200 800

## APPEND ONLY MODE

### appendonly 

* appendonly no
* 指定是否在每次更新操作后进行日志操作

### appendfilename

* appendfilename "appendonly.aof"
* aof文件名

### appendfsync

* appendfsync everysec
* no：表示等操作系统进行数据缓存同步到磁盘（快）
* always：表示每次更新操作后手动调用fsync将数据写到磁盘（慢，安全）
* everysec：表示每秒同步一次（折衷，默认值）

### no-appendfsync-on-rewrite

* no-appendfsync-on-rewrite no

### auto-aof-rewrite-percentage

* auto-aof-rewrite-percentage 100

### auto-aof-rewrite-min-size

* auto-aof-rewrite-min-size 64mb

### aof-load-truncated

* aof-load-truncated yes

### aof-use-rdb-preamble

* aof-use-rdb-preamble yes

## LUA SCRIPTING

### lua-time-limit
* lua-time-limit 5000

## REDIS CLUSTER

### cluster-enabled

* cluster-enabled yes

### cluster-config-file

* cluster-config-file nodes-6379.conf

### cluster-node-timeout

* cluster-node-timeout 15000

### cluster-replica-validity-factor

* cluster-replica-validity-factor 10

### cluster-migration-barrier 

* cluster-migration-barrier 1

### cluster-require-full-coverage

* cluster-require-full-coverage yes

### cluster-replica-no-failover

* cluster-replica-no-failover no

### cluster-allow-reads-when-down

* cluster-allow-reads-when-down no

## SLOW LOG

###  slowlog-log-slower-than 

* slowlog-log-slower-than 10000

### slowlog-max-len

* slowlog-max-len 128

## LATENCY MONITOR

### latency-monitor-threshold

* latency-monitor-threshold 0

## EVENT NOTIFICATION

### PUBLISH

* PUBLISH __keyspace@0__:foo del

### notify-keyspace-events

* notify-keyspace-events Elg

### notify-keyspace-events

* notify-keyspace-events ""

## GOPHER SERVER

### gopher-enabled 

* gopher-enabled no

## ADVANCED CONFIG

### hash-max-ziplist-entries

* hash-max-ziplist-entries 512

### hash-max-ziplist-value

* hash-max-ziplist-value 64

### list-max-ziplist-size

* list-max-ziplist-size -2

### list-compress-depth

* list-compress-depth 0

### set-max-intset-entries

* set-max-intset-entries 512

### zset-max-ziplist-entries

* zset-max-ziplist-entries 128

### zset-max-ziplist-value 

* zset-max-ziplist-value 64

### hll-sparse-max-bytes

* hll-sparse-max-bytes 3000

### stream-node-max-bytes

* stream-node-max-bytes 4096

### stream-node-max-entries

* stream-node-max-entries 100

### activerehashing

* activerehashing yes

### client-output-buffer-limit

* client-output-buffer-limit normal 0 0 0
* client-output-buffer-limit replica 256mb 64mb 60
* client-output-buffer-limit pubsub 32mb 8mb 60

### client-query-buffer-limit

* client-query-buffer-limit 1gb

### proto-max-bulk-len 

* proto-max-bulk-len 512mb

### hz

* hz 10

### dynamic-hz

* dynamic-hz yes

### aof-rewrite-incremental-fsync

* aof-rewrite-incremental-fsync yes

### rdb-save-incremental-fsyn

* rdb-save-incremental-fsyn yes

### lfu-log-factor

* lfu-log-factor 10

### lfu-decay-time

* lfu-decay-time 1

## ACTIVE DEFRAGMENTATION

### activedefrag 

* activedefrag no

### active-defrag-ignore-bytes

* active-defrag-ignore-bytes 100mb

### active-defrag-threshold-lower 

* active-defrag-threshold-lower 10

### active-defrag-threshold-upper

* active-defrag-threshold-upper 100

### active-defrag-cycle-min

* active-defrag-cycle-min 1

### active-defrag-cycle-max

* active-defrag-cycle-max 25

### active-defrag-max-scan-fields

* active-defrag-max-scan-fields 1000

### jemalloc-bg-thread

* jemalloc-bg-thread yes

### server_cpulist 

* server_cpulist 0-7:2

### bio_cpulist

* bio_cpulis 1,3

### aof_rewrite_cpulist 

* aof_rewrite_cpulist 8-11

### bgsave_cpulist

* bgsave_cpulist 1,10-11