---
title: Redis地理位置
date: 2021-02-22
categories:
 - Redis
tags:
 - Redis
---

## Redis地理位置

### GEOADD

:::tip
GEOADD key longitude latitude member [longitude latitude member ...]  
可用版本： >= 3.2.0   
时间复杂度： 每添加一个元素的复杂度为 O(log(N)) ， 其中 N 为键里面包含的位置元素数量。
:::

将给定的空间元素（纬度、经度、名字）添加到指定的键里面。

* 有效的经度介于 -180 度至 180 度之间。

* 有效的纬度介于 -85.05112878 度至 85.05112878 度之间。

```shell script
redis> GEOADD Sicily 13.361389 38.115556 "Palermo" 15.087269 37.502669 "Catania"
(integer) 2

redis> GEODIST Sicily Palermo Catania
"166274.15156960039"

redis> GEORADIUS Sicily 15 37 100 km
1) "Catania"

redis> GEORADIUS Sicily 15 37 200 km
1) "Palermo"
2) "Catania"
```

### GEOPOS

:::tip
GEOPOS key member [member ... ]   
可用版本： >= 3.2.0  
时间复杂度： 获取每个位置元素的复杂度为 O(log(N)) ， 其中 N 为键里面包含的位置元素数量。
:::

从键里面返回所有给定位置元素的位置（经度和纬度）。

```shell script
redis> GEOADD Sicily 13.361389 38.115556 "Palermo" 15.087269 37.502669 "Catania"
(integer) 2

redis> GEOPOS Sicily Palermo Catania NonExisting
1) 1) "13.361389338970184"
   2) "38.115556395496299"
2) 1) "15.087267458438873"
   2) "37.50266842333162"
3) (nil)
```

### GEODIST

:::tip
GEODIST key member1 member2 [unit]   
可用版本： >= 3.2.0  
复杂度： O(log(N))
:::

返回两个给定位置之间的距离。

指定单位的参数 unit 必须是以下单位的其中一个：

* m 表示单位为米。

* km 表示单位为千米。

* mi 表示单位为英里。

* ft 表示单位为英尺

```shell script
redis> GEOADD Sicily 13.361389 38.115556 "Palermo" 15.087269 37.502669 "Catania"
(integer) 2

redis> GEODIST Sicily Palermo Catania
"166274.15156960039"

redis> GEODIST Sicily Palermo Catania km
"166.27415156960038"

redis> GEODIST Sicily Palermo Catania mi
"103.31822459492736"

redis> GEODIST Sicily Foo Bar
(nil)
```

### GEORADIUS

:::tip
GEORADIUS key longitude latitude radius m|km|ft|mi [WITHCOORD] [WITHDIST] [WITHHASH] [ASC|DESC] [COUNT count]         
可用版本： >= 3.2.0  
时间复杂度： O(N+log(M))， 其中 N 为指定半径范围内的位置元素数量， 而 M 则是被返回位置元素的数量
:::

* 以给定的经纬度为中心， 返回键包含的位置元素当中， 与中心的距离不超过给定最大距离的所有位置元素

在给定以下可选项时， 命令会返回额外的信息：

* WITHDIST ： 在返回位置元素的同时， 将位置元素与中心之间的距离也一并返回。 距离的单位和用户给定的范围单位保持一致。

* WITHCOORD ： 将位置元素的经度和维度也一并返回。

* WITHHASH ： 以 52 位有符号整数的形式， 返回位置元素经过原始 geohash 编码的有序集合分值。 这个选项主要用于底层应用或者调试， 实际中的作用并不大。

* ASC ： 根据中心的位置， 按照从近到远的方式返回位置元素。

* DESC ： 根据中心的位置， 按照从远到近的方式返回位置元素。

```shell script
redis> GEOADD Sicily 13.361389 38.115556 "Palermo" 15.087269 37.502669 "Catania"
(integer) 2

redis> GEORADIUS Sicily 15 37 200 km WITHDIST
1) 1) "Palermo"
   2) "190.4424"
2) 1) "Catania"
   2) "56.4413"

redis> GEORADIUS Sicily 15 37 200 km WITHCOORD
1) 1) "Palermo"
   2) 1) "13.361389338970184"
      2) "38.115556395496299"
2) 1) "Catania"
   2) 1) "15.087267458438873"
      2) "37.50266842333162"

redis> GEORADIUS Sicily 15 37 200 km WITHDIST WITHCOORD
1) 1) "Palermo"
   2) "190.4424"
   3) 1) "13.361389338970184"
      2) "38.115556395496299"
2) 1) "Catania"
   2) "56.4413"
   3) 1) "15.087267458438873"
      2) "37.50266842333162"

```

### GEORADIUSBYMEMBER 

:::tip
GEORADIUSBYMEMBER key member radius m|km|ft|mi [WITHCOORD] [WITHDIST] [WITHHASH] [ASC|DESC] [COUNT count]   
可用版本： >= 3.2.0   
时间复杂度： O(log(N)+M)， 其中 N 为指定范围之内的元素数量， 而 M 则是被返回的元素数量。
:::

这个命令和 GEORADIUS 命令一样， 都可以找出位于指定范围内的元素

```shell script
redis> GEOADD Sicily 13.583333 37.316667 "Agrigento"
(integer) 1

redis> GEOADD Sicily 13.361389 38.115556 "Palermo" 15.087269 37.502669 "Catania"
(integer) 2

redis> GEORADIUSBYMEMBER Sicily Agrigento 100 km
1) "Agrigento"
2) "Palermo"
```