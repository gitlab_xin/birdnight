---
title: Redis的持久化
date: 2021-01-10
categories:
 - Redis
tags:
 - Redis
---

# Rdis的持久化

## Redis的持久性

Redis提供了不同的持久性选型：

* RDB持久性以指定的时间间隔执行数据集的时间快照
* AOF持久性记录服务器接收的每个写入操作，将在服务器启动是再次播放，重建原始数据集
* 可以在同一实例当中组合AOF和RDB，请注意，在这种情况下，当Redis要重新启动时，AOF文件将用于重建原始数据集，因为它保证是完整的

## RDB（Redis DataBase）

### 优缺点

RDB优势：

* RDB是Redis数据的一个非常紧凑的单文件时间点表示，RDB	文件非常适合备份（例如可以在最近24小时内每小时归纳您的RDB文件，并且每天保存RDB文件快照30天。这便于在灾难时轻松恢复数据集的不同版本）。
* RDB非常适合灾难恢复
* RDB最大限度的提高了Redis的性能，因为Redis父进程为了坚持不懈而需要做的一件事就是分配一个任务给将完成其余工作的孩子，父实例永远不会执行磁盘I/O类型操作
* 与AOF相比，RDB允许使用大数据集更快的重启

RDB劣势：

* 没办法做到实时持久化/秒级持久化。
* Redis意外down掉的话，会丢失最后一次快照后的所有修改
* Fork的时候，内存中的数据被克隆了一份，大致2倍的膨胀性。

### 相关配置

* redis.conf默认是只开启RDB. 
* save 指定在规定时间内，有多少次更新操作，就将数据同步到数据库文件，可以多个配合
* rdbcompression 指定存储至本地数据库时是否进行压缩数据，Redis采用LZF压缩
* rdbchecksum 是否进行CRC64校验rdb文件，会有一定的性能损失
* dbfilename 指定本地数据库的rdb文件名
* dir 指定本地数据库存放目录

### 如何触发

修改redis.conf配置
* save 900 1   
* save 300 10   
* save 60 2   
* dbfilename dump-docker.rdb 

触发前文件大小

```
bash-3.2# ls -ll
total 8
-rw-r--r--  1 macxin  staff  113  1 10 15:48 dump-docker.rdb
```

触发命令

```$xslt
127.0.0.1:6379> mset k3 v3 k4 v4

127.0.0.1:6379> keys *
1) "k1"
2) "k2"
3) "k3"
4) "k4"
```

触发后文件大小

```
bash-3.2# ls -ll
total 8
-rw-r--r--  1 macxin  staff  127  1 10 15:57 dump-docker.rdb
```

可以看出两个文件大小发生了变化，当满足60秒内修改了2个操作（删除，修改，新增）后，会进行一次存储

### 如何恢复

#### 准备工作

1、停止redis-cli（shutdown 或者 docker stop containerid）  
2、切换到保存的rdb文件目录，也就是存放dump-docker.rdb的目录下   
3、将rdb文件进行复制一份且删除掉（mv dump-docker.rdb dump-docker.rdb.temp）   

* 结果如下

```$xslt
bash-3.2# ls
dump-docker.rdb
```

### 恢复

```$xslt
127.0.0.1:6379> keys *
(empty array)
```

1、重启（redis-server）我们的redis会发现，之前keys已经消失不见，这个时候就可以进行恢复操作   
2、停止redis-cli   
3、同样切到rdb文件目录(shutdown 或者 docker stop containerid都会产生rdb文件，需要进行删除, rm -rf dump-docker.rdb)   
4、将原本的rdb文件名修改回来（mv dump-docker.rdb.temp dump-docker.rdb）   
5、启动redis-cli（redis-server）   

* 结果如下，这个时候数据已经进行恢复

```$xslt
127.0.0.1:6379> keys *
1) "k1"
2) "k2"
3) "k3"
4) "k4"
```

### 使用情况

* 适合大规模的数据恢复
* 对数据完整性和一致性要求不高

## AOF（Append Only File）

:::tip
以日志的形式来记录每个写操作，将Redis执行过的所有写指令记录下来，只许追加文件但不可以改写文件，Redis启动之初会读取该文件重新构建数据。
:::

### 优缺点

AOF优势：

* AOF更持久
* 自动重写AOF。重写是完全安全的，Redis太大时，当Redis继续附加到旧文件，使用创建当前数据集所需的最小操作集生成一个全新的文件，并且一旦第二个文件准备就绪，Redis会切换两个文件并开始附加到新的那一个
* AOF以易于理解和解析的格式一个接一个地包含所有操作的日志。您甚至可以轻松导出AOF文件。例如，即使您使用FLUSHALL命令刷新了所有错误，如果在此期间未执行重写日志，您仍然可以保存数据集，只需停止服务器，删除最新命令，然后重启Redis

AOF缺点：

* AOF文件通常比同一数据集的等效RDB大
* 根据确切的fsync策略，AOF可能比RDB慢。

### 相关配置

* appendonly 指定是否在每次更新操作后进行日志操作
* appendfilename aof文件名
* appendfsync 同步策略（默认always， 表示每次更新操作后手动调用fsync将数据写到磁盘）

|  表头   | 表头  |  表头   | 表头  |
|  ----  | ----  |  ----  | ----  |
|  命令  | always  |  everysec  | no  |
|  优点  | 不丢失数据  |  每秒一次fsync丟1秒数据  | 不用管  |
|  缺点  | IO开销比较大  |  丟1秒数据  | 不可控  |


### 正常恢复

* AOF与RDB的恢复过程一致，这里不重复展示
* 启动：修改默认的appendonly为yes
* 将现有数据的AOF文件复制到一份保存到对应目录
* 恢复：重启Redis然后进行加载

### 异常恢复

* 修复: Redis-check-aof --fix 进行修复
* 恢复：重启Redis然后进行加载

## 如何选择

* RDB持久化方式能够在指定时间间隔对数据进行快照存储
* AOF持久化方式记录每次对服务器写的操作，当服务器重启的时候会重新执行这些命令来恢复原始的数据，

### 同时开启两种方式

* 这种情况下会优先载入AOF文件来恢复原始的数据，通常情况下AOF会比RDB更完整。

|  表头   | RDB  |  AOF   | 
|  ----  | ----  |  ----  | 
|  启动优先级  | 低  |  高  | 
|  体积  | 小  |  大  | 
|  恢复速度  | 快  |  慢  | 
|  数据安全性  | 丢数据  |  根据策略决定  | 
|  轻重  | 重  |  轻  | 













