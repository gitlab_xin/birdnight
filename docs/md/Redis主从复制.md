---
title: Redis主从复制
date: 2021-01-16
categories:
 - Redis
tags:
 - Redis
sidebar: auto
---

## Redis主从复制

:::tip
可以实现当一台数据库中的数据更新后，自动将更新的内容数据同步到另一台数据库上。
:::

### 原理

<img :src="$withBase('/img/2207930092-d246b084cae752a6_articlex.png')">

* 从服务器启动成功后，连接主数据库后，发送sync指令；
* 主数据库接受到命令后，开始执行bgsave命令生成RDB快照文件，并使用缓存区记录之后的所有命令写命令；
* 主数据bgsave完成后，向所有从数据库发送快照文件，并在发送期间继续记录被执行的写命令；
* 从数据库收到快照文件丢弃所有旧数据，载入收到的快照；
* 主数据快照发送完毕后开始向从数据库发送缓存区的写命令；
* 从数据库完成对快照的载入，开始接受命令请求，并执行来自主数据库缓存区的写命令（从数据库初始化完成）；
* 主数据库每执行一个写命令就会向所有的从数据库发送相同的命令，从数据库接收并执行收到的写的命令（从数据库初始化完成之后的操作）；

### 优缺点

优点：

* 支持主从分离，可以支持读写分离；
* 分担主数据库压力（master以写为主，slave以读为主）；

缺点：

* Redis不具备自动容错和恢复功能；
* 主机宕机，宕机前有部分数据未能同步到从机上，降低数据的一致性；
* 当多个从机重启的时候，可能会导致主机I/O剧增，从而宕机
* Redis 较难支持在线扩容

### 如何使用

* 配主不配从

* 配置命令核心 【replicaof masterip port】

* 步骤

1、复制多份redis.conf  
2、配置主服务器 replicaof masterip port   
3、开启 daemonize   
4、修改pid文件名称   
5、指定端口  
6、log文件名   
7、dump.rdb文件名  

### 一主二仆

#### 配置（配从不配主）

master配置(redis.conf)（默认）

```$xslt
port 6379

#主机与从机在同一台机器上无需改动
bind 127.0.0.1
```


slave配置(redis_slave_1.conf)如下

```$xslt
port 6381

replicaof 127.0.0.1 6379

```

slave配置（redis_slave_2.conf）如下

```$xslt
port 6382

replicaof 127.0.0.1 6379

```
分别运行三个redis实例(.conf所在目录)

```$xslt
redis-server /etc/redis.conf
redis-server /etc/redis_slave_1.conf
redis-server /etc/redis_slave_2.conf

```

分别运行成功

```$xslt
#此时会进入到6379端口的主redis中
redis-cli

#执行info replication 查看状态
info replication

# Replication
role:master
connected_slaves:2
slave0:ip=127.0.0.1,port=6382,state=online,offset=2793695,lag=1
slave1:ip=127.0.0.1,port=6381,state=online,offset=2793695,lag=1
master_replid:c2f889dabe541e9e362432486ef7b4ee964314df
master_replid2:e15ecfdb4fa0ae9a03609533f68faca66bc4e26f
master_repl_offset:2793695
second_repl_offset:2339082
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1745120
repl_backlog_histlen:104857
```

此时可以看的当前6379端口的角色为master，且有2个从机（connected_slaves:2）以及从机端口、IP等等

```$xslt

#离开6379的redis实例
quit

#进入其中一个从机
redis-cli -p 6381

info replication

# Replication
role:slave
master_host:127.0.0.1
master_port:6379
master_link_status:up
master_last_io_seconds_ago:1
master_sync_in_progress:0
slave_repl_offset:2814065
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:c2f889dabe541e9e362432486ef7b4ee964314df
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:2814065
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:2342789
repl_backlog_histlen:471277

```

此时可以看的当前6379端口的角色为slave，且有主机的种种连接信息

## 哨兵模式

:::tip
哨兵作为一个独立的进程，通过发送命令，等待Redis服务器响应，从而监控运行的多个Redis实例
:::

### 优缺点

优点：

* 哨兵模式是基于主从模式的，所有主从的优点，哨兵模式都具有。
* 主从可以自动切换

缺点：

* Redis较难支持在线扩容

### 配置

* 新增哨兵配置文件

```$xslt
vim sentinel.conf

# 配置监听的主服务器，这里sentinel monitor代表监控，mymaster代表服务器的名称，可以自定义
# 127.0.0.1代表监控的主服务器，6379代表端口，1代表只有一个或一个以上的哨兵认为主服务器不可用的时候，才会进
  行failover操作
sentinel monitor mymaster 127.0.0.1 6379 1

#启动哨兵模式
redis-sentinel sentinel.conf

#出现以下信息，则说明开启成功
37:X 16 Jan 2021 08:47:12.430 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
37:X 16 Jan 2021 08:47:12.430 # Redis version=6.1.240, bits=64, commit=00000000, modified=0, pid=37, just started
37:X 16 Jan 2021 08:47:12.430 # Configuration loaded
37:X 16 Jan 2021 08:47:12.432 * monotonic clock: POSIX clock_gettime
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 6.1.240 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                   
 (    '      ,       .-`  | `,    )     Running in sentinel mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 26379
 |    `-._   `._    /     _.-'    |     PID: 37
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           http://redis.io        
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               

37:X 16 Jan 2021 08:47:12.434 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
37:X 16 Jan 2021 08:47:12.444 # Could not rename tmp config file (Resource busy)
37:X 16 Jan 2021 08:47:12.445 # WARNING: Sentinel was not able to save the new configuration on disk!!!: Invalid argument
37:X 16 Jan 2021 08:47:12.445 # Sentinel ID is 43646a833f28044d56b0db5ed09b2f6790b14257
37:X 16 Jan 2021 08:47:12.445 # +monitor master mymaster 127.0.0.1 6379 quorum 1
37:X 16 Jan 2021 08:47:12.450 * +slave slave 127.0.0.1:6382 127.0.0.1 6382 @ mymaster 127.0.0.1 6379
37:X 16 Jan 2021 08:47:12.458 # Could not rename tmp config file (Resource busy)
37:X 16 Jan 2021 08:47:12.458 # WARNING: Sentinel was not able to save the new configuration on disk!!!: Invalid argument
37:X 16 Jan 2021 08:47:12.459 * +slave slave 127.0.0.1:6381 127.0.0.1 6381 @ mymaster 127.0.0.1 6379
37:X 16 Jan 2021 08:47:12.466 # Could not rename tmp config file (Resource busy)
37:X 16 Jan 2021 08:47:12.467 # WARNING: Sentinel was not able to save the new configuration on disk!!!: Invalid argument

```

* 测试哨兵是否有效

```$xslt
#进入我们的主redis实例
redis-cli

#关掉主redis实例
shutdown

#进入任意一个从redis实例
redis-cli -p 6381

#查看此时的主从状态
info replication

# Replication
role:slave
master_host:127.0.0.1
master_port:6382
master_link_status:up
master_last_io_seconds_ago:1
master_sync_in_progress:0
slave_repl_offset:12751
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:6ef0fde4e423825f986d44cf4401ddae5e3cbc8d
master_replid2:11cd7c992e5d1477668ee712e0681340dc41876d
master_repl_offset:12751
second_repl_offset:12050
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:15
repl_backlog_histlen:12737

```

可以看出哨兵已经生效，此时6381实例的主服务器已经更改成6382的实例，

* 恢复刚刚关闭的6379的redis实例

```
#恢复6379的redis实例（注意路径）
redis-server redis.conf

#进入6382的redis实例（上一步被哨兵选举成主redis的实例，实际操作不一定是6382的实例）
redis-cli -p 6382

#查询主从信息
info replication

# Replication
role:master
connected_slaves:2
slave0:ip=127.0.0.1,port=6381,state=online,offset=40067,lag=1
slave1:ip=127.0.0.1,port=6379,state=online,offset=40067,lag=0
master_replid:6ef0fde4e423825f986d44cf4401ddae5e3cbc8d
master_replid2:11cd7c992e5d1477668ee712e0681340dc41876d
master_repl_offset:40067
second_repl_offset:12050
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:40067


```

可以看出，在哨兵模式下，当主实例宕机期间，哨兵会选举新的主redis实例，即使原本的redis实例恢复，也是变成其中一个从机。
















