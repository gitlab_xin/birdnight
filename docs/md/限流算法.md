---
title: 限流算法
date: 2020-9-17
categories:
 - Computer
tags:
 - Algorithm
---

# 限流算法

## 计数器算法
> 主要通过一个或多个计数器来统计一段时间内的请求数量，然后判读是否超过限流，超过则限流，不超过则对应的计数器数量加一；

> 计数器分为两种，固定窗口和滑动窗口算法
 
### 固定窗口
> 统计一个固定的时间窗口内的请求总数来判断是否进行限流；  

```
/**
 * 固定窗口计数器限流算法
 * @param $key string 限流依据，例如uid，url等
 * @param $time int 限流时间段，单位秒
 * @param $limit int 限流总数
 */
function limit($key, $time, $limit) {

    //当前时间所在分片
    $current_segment=floor(time() / $time);
    //按当前时间和限流参数生成key
    $current_key = $key . '_' . $time . '_' . $limit . '_' . $current_segment;

    $redis = new Redis();
    //key不存在才设置，且设置过期时间
    $redis->set($current_key, 0, ['nx', 'ex' => $time]);
    $current = $redis->incr($current_key);

    //为了解决请求并发的问题，代码实现上要先加再判断
    if ($current > $limit) {
        return false;
    }
    return true;
}
```
> 缺陷：实现虽然简单，但是会有一个临界突刺的问题，最后一秒和最开始一秒的流量集中到一起，会出现大量流量；

<img :src="$withBase('/img/w22iUU.md.png')">

### 滑动窗口
> 滑动窗口是固定窗口的一个优化版，主要有两个特点:  
 
*  划分成多个小的时间段，各时间段进行一个计算；  
* 根据当前时间，动态的往前滑动来计算时间窗口范围，并合并计算；

> PHP逻辑代码

```
/**
 * 滑动窗口计数器限流算法
 * @param $key string 限流依据，例如uid，url等
 * @param $time int 限流时间段，单位秒
 * @param $limit int 限流总数
 * @param $segments_num int 分段个数
 */
function limit($key, $time, $limit, $segments_num) {

    //小分片时间长度
    $segments_time=floor($time/$segments_num);
    //当前时间所在小分片
    $current_segment=floor(time() / $segments_time);
    //按限流时间段生成key
    $current_key = $key . '_' . $time . '_' . $limit . '_' . $current_segment;
    
    $redis = new Redis();
    //先更新当前时间所在小分片计数，key不存在才设置，且设置过期时间
    $redis->set($current_key, 0, ['nx', 'ex' => $time]);
    $current = $redis->incr($current_key);

    for($window=$segments_time;$window<$time;$window+=$segments_time){
        $current_segment=$current_segment-1;
        $tmp_key = $key . '_' . $time . '_' . $limit . '_' . $current_segment;
        //计算时间窗口内的总数
        $current+=intval($redis->get($tmp_key));
        if ($current > $limit) {
            //超过限制的话要回滚本次加的次数
            $redis->decr($current_key);
            return false;
        }
    }
    
    return true;
}
```

<img :src="$withBase('/img/w22PET.md.png')">

> 可以看到，每次时间往后，窗口也会动态往后滑动，从而丢弃一些更早期的计数数据，从而实现总体计数的平稳过度。当滑动窗口划分的格子越多，那么滑动窗口的滑动就越平滑，限流的统计就会越精确。事实上，固定窗口算法就是只划分成一个格子的滑动窗口算法。

>缺陷：滑动窗口限流算法虽然可以保证任意时间窗口内接口请求次数都不会超过最大限流值，但是相对来说对系统的瞬时处理能力还是没有考虑到，无法防止在更细的时间粒度上访问过于集中的问题，例如在同一时刻（同一秒）大量请求涌入，还是可能会超过系统负荷能力。

## 漏桶算法
>漏桶算法就是一种从系统的处理能力出发进行限流算法；类似生活中用的漏斗，上面进水，下面有个下口出水，当请求进来时，相当于水倒入漏斗，然后从下端小口慢慢地均匀流出；不管上面流量多大，下面流出的速度始终不变，超出漏斗容量的则丢弃。漏桶算法以固定的速率释放访问请求，直到漏斗为空；

<img :src="$withBase('/img/w22F5F.md.png')">

> 缺陷：漏桶算法的缺陷很明显，由于出口的处理速率是固定的，当短时间内有大量的突发事件时，即便此时服务器没有任何负载，每个请求也都得在队列中等待一段时间才能被响应，因此漏桶算法无法应对突发流量。


## 令牌算法
 > 令牌桶算法也是一个桶， 但是它不是通过控制限制漏出的请求来控制流量，而是通过控制桶的令牌的生成数量来达到限流的目的。令牌桶定时往里面丢一定的令牌，令牌桶满了就不再往里面加令牌；每一个请求就要先在桶里拿一个令牌，拿到令牌则通过，拿不到则拒绝；
 
 <img :src="$withBase('/img/w2fZvj.md.png')">
 
 >JAVA 逻辑代码
 
 ```
 <!--引入maven依赖-->
 <dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>29.0-jre</version>
</dependency>
 ```
 
>Guava是Java领域优秀的开源项目，它包含了Google在Java项目中使用一些核心库，包含集合(Collections)，缓存(Caching)，并发编程库(Concurrency)，常用注解(Common annotations)，String操作，I/O操作方面的众多非常实用的函数。 Guava的 RateLimiter提供了令牌桶算法实现：平滑突发限流(SmoothBursty)和平滑预热限流(SmoothWarmingUp)实现。
 
<img :src="$withBase('/img/wWdblV.md.jpg')">

 
### 平滑突发限流
>使用RateLimiter的静态方法创建一个限流器，设置每秒放置的令牌数为n个。返回的RateLimiter对象可以保证1秒内不会给超过n个令牌，并且以固定速度进行放置，达到平滑输出的效果。  

```
//设置每秒放入6个令牌
RateLimiter r = RateLimiter.create(6);
//acquire(2)，每次拿取2个令牌
System.out.println("get 2 tokens: " + r.acquire(3) + "s"); //0.0s
System.out.println("get 2 tokens: " + r.acquire(3) + "s"); //0.494122s
System.out.println("get 2 tokens: " + r.acquire(2) + "s"); //0.493491s
System.out.println("get 2 tokens: " + r.acquire(2) + "s"); //0.328664s
```
>运行结果. 
 
```
get 2 tokens: 0.0s
get 2 tokens: 0.494122s
get 2 tokens: 0.493491s
get 2 tokens: 0.328664s

Process finished with exit code 0
```
> 由于每秒生产6个令牌，即放置1个令牌大致需要1/6的时间，3个令牌则需要0.5s的时间。
> RateLimiter由于会积累令牌，所以可以应对突发流量。当某一个请求需要5个令牌，由于此时令牌桶中已经积累足够的令牌，足以应对响应。

```
//同样生产6个令牌
RateLimiter r = RateLimiter.create(6);
System.out.println("get 3 tokens: " + r.acquire(3) + "s"); //0.0s
try {
	Thread.sleep(2000);
} catch (InterruptedException e) {
	e.printStackTrace();
}
System.out.println("get 3 tokens: " + r.acquire(3) + "s"); //0.0s
System.out.println("get 3 tokens: " + r.acquire(3) + "s"); //0.0s
System.out.println("get 3 tokens: " + r.acquire(3) + "s"); //0.0s
System.out.println("get 2 tokens: " + r.acquire(2) + "s"); //0.49877s
```
>运行结果  

```
get 3 tokens: 0.0s
get 3 tokens: 0.0s
get 3 tokens: 0.0s
get 3 tokens: 0.0s
get 2 tokens: 0.49877s

Process finished with exit code 0
```
>RateLimiter在没有足够令牌发放时，采用滞后处理的方式，也就是前一个请求获取令牌所需等待的时间由下一次请求来承受，也就是代替前一个请求进行等待。

### 平滑预热限流
>RateLimiter的 SmoothWarmingUp是带有预热期的平滑限流，它启动后会有一段预热期，逐步将分发频率提升到配置的速率。 比如下面代码中的例子，创建一个平均分发令牌速率为2，预热期为3分钟。由于设置了预热时间是3秒，令牌桶一开始并不会0.5秒发一个令牌，而是形成一个平滑线性下降的坡度，频率越来越高，在3秒钟之内达到原本设置的频率，以后就以固定的频率输出。这种功能适合系统刚启动需要一点时间来“热身”的场景

```
//每秒生产2个，预热3秒
RateLimiter r1 = RateLimiter.create(2, 3, TimeUnit.SECONDS);

System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.0s
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //1.327855s
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.996388s
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.663468s
System.out.println("end");
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.494365s
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.494203s
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.496428s
System.out.println("get 1 tokens: " + r1.acquire(1) + "s"); //0.494948s
```

 >运行结果
 
 ```
get 1 tokens: 0.0s
get 1 tokens: 1.327855s
get 1 tokens: 0.996388s
get 1 tokens: 0.663468s
end
get 1 tokens: 0.494365s
get 1 tokens: 0.494203s
get 1 tokens: 0.496428s
get 1 tokens: 0.494948s

Process finished with exit code 0
 ```
 
 
## 方法摘要
 
 
|  修饰符和类型   | 方法和描述  |
|  ----  | ----  |
| double  | acquire() 从RateLimiter获取一个许可，该方法会被阻塞直到获取到请求 |
| double  | acquire(int permits) 从RateLimiter获取指定许可数，该方法会被阻塞直到获取到请求 |
| static RateLimiter  | acreate(double permitsPerSecond)根据指定的稳定吞吐率创建RateLimiter，这里的吞吐率是指每秒多少许可数（通常是指QPS，每秒多少查询） |
| static RateLimiter  | create(double permitsPerSecond, long warmupPeriod, TimeUnit unit)根据指定的稳定吞吐率和预热期来创建RateLimiter，这里的吞吐率是指每秒多少许数（通常是指QPS，每秒多少个请求量），在这段预热时间内，RateLimiter每秒分配的许可数会平稳地增长直到预热期结束时达到其最大速率。（只要存在足够请求数来使其饱和））|
| double  | getRate() 返回RateLimiter 配置中的稳定速率，该速率单位是每秒多少许可数|
| void  | setRate(double permitsPerSecond) 更新RateLimite的稳定速率，参数permitsPerSecond 由构造RateLimiter的工厂方法提供。|
| String  | toString() 返回对象的字符表现形式|
| boolean  | tryAcquire() 从RateLimiter 获取许可，如果该许可可以在无延迟下的情况下立即获取得到的话|
| boolean  | tryAcquire(int permits) 从RateLimiter 获取许可数，如果该许可数可以在无延迟下的情况下立即获取得到的话|
| boolean  | tryAcquire(int permits, long timeout, TimeUnit unit) 从RateLimiter 获取指定许可数如果该许可数可以在不超过timeout的时间内获取得到的话，或者如果无法在timeout 过期之前获取得到许可数的话，那么立即返回false （无需等待）|
| boolean  | tryAcquire(long timeout, TimeUnit unit)从RateLimiter 获取许可如果该许可可以在不超过timeout的时间内获取得到的话，或者如果无法在timeout 过期之前获取得到许可的话，那么立即返回false（无需等待）|




 
 
 
