---
title: Cluster进行故障转移、重新分片
date: 2021-02-14
categories:
 - Redis
tags:
 - Redis
---

## redis.conf准备

修改redis.conf
```shell script
# 开放所有地址
bind 0.0.0.0

# 绑定自定义端口
port 7000 

# 开启集群 把注释#去掉
cluster-enabled yes

# 集群的配置 配置文件首次启动自动生成
cluster-config-file nodes_7000.conf 

# 开启aof
appendonly yes

# 要宣布的IP地址。nat模式要指定宿主机IP
cluster-announce-ip 8.135.52.239  

# 要宣布的数据端口
cluster-announce-port 7001

# 要宣布的集群总线端口 + 10000
cluster-announce-bus-port 17001
```

## 程序准备(需要安装redis扩展的php环境)

```php
<?php
namespace app\php;

use RedisCluster;

$host = [
  '8.135.52.239:7004'
];
$redisCluster = new RedisCluster(NULL, $host);

for ($x = 0; $x < 10000000; $x++){
    $k = 'user-1-k-'.$x;
    $v = 'user-1-v-'.$x;
    $redisCluster->set($k, $v);
    echo "SET {$k} {$v}".PHP_EOL;

    $k = 'user-2-k-'.$x;
    $v = 'user-2-v-'.$x;
    $redisCluster->set($k, $v);
    echo "SET {$k} {$v}".PHP_EOL;
}
```

::: tip
需要开启对应端口号（7000、17000、7001、17001）
:::

redis集群总线(17000、17001作用)

::: tip
redis集群总线端口为redis客户端端口加上10000，比如说你的redis 6379端口为客户端通讯端口，那么16379端口为集群总线端口
:::

<img :src="$withBase('/img/WechatIMG754.png')">

开启集群
::: tip
 --replicas参数指定集群中每个主节点配备几个从节点，这里设置为1。
:::
```shell script
docker run -it --net host inem0o/redis-trib create --replicas 1 8.135.52.239:7000 8.135.52.239:7001 8.135.52.239:7002 8.135.52.239:7003 8.135.52.239:7004 8.135.52.239:7005
```

## 测试集群连接

```shell script
redis-cli -c -p host:port 

#连接成功
8.135.52.239:7004>

#集群操作成功
8.135.52.239:7004> set k4 v4
-> Redirected to slot [8455] located at 8.135.52.239:7001
OK
8.135.52.239:7001>

#集群操作失败（检查对应集群总线端口是否开启）
8.135.52.239:7000> set k1 v1
-> Redirected to slot [12706] located at 127.0.0.1:7002
(error) CLUSTERDOWN The cluster is down
```

## 对集群进行重新分片

:::tip
在执行重新分片的过程中， 请让你的 php 程序处于运行状态， 这样你就会看到， 重新分片并不会对正在运行的集群程序产生任何影响
:::

### 分片命令(reshard)

```shell script
# 你只需要指定集群中其中一个节点的地址， redis-trib 就会自动找到集群中的其他节点。
docker run -it --net host inem0o/redis-trib reshard 8.135.52.239:7000

#执行 redis-trib 的第一步就是设定你打算移动的哈希槽的数量
>>> Performing Cluster Check (using node 8.135.52.239:7000)
M: 846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
S: 5d3bf967e589615df6863e0dc8dd154ee83311d3 8.135.52.239:7005@17005
   slots: (0 slots) slave
   replicates 878176fcd293a3fb66a4b67b54f104503e6041ba
S: 1077e8df06eef691432edd01d677f9f26018ebf5 8.135.52.239:7004@17004
   slots: (0 slots) slave
   replicates f5852f7fd2e4f4e93803637bb8ff57408a30f488
M: f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
M: 878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
S: 1f90f143f6ed56ed315bfec49b5af61a092f0c03 8.135.52.239:7003@17003
   slots: (0 slots) slave
   replicates 846afdad3b4cf5e1339113eadcd76c9976354b8a
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
How many slots do you want to move (from 1 to 16384)? 10
```

:::tip
我们将打算移动的槽数量设置为 10 个， 如果 php 程序一直运行着的话， 现在 10 个槽里面应该有不少键了。
:::

```shell script

# 指定目标需要使用节点的 ID ， 而不是 IP 地址和端口。
# 比如说， 我们打算使用集群的第一个主节点来作为目标， 
# 它的 IP 地址和端口是 127.0.0.1:7000 ， 
# 而节点 ID 则是 f5852f7fd2e4f4e93803637bb8ff57408a30f488 ， 
# 那么我们应该向 redis-trib 提供节点的 ID ：
What is the receiving node ID? f5852f7fd2e4f4e93803637bb8ff57408a30f488

```

接着， redis-trib 会向你询问重新分片的源节点（source node）， 也即是， 要从哪个节点中取出 10 个哈希槽， 并将这些槽移动到目标节点上面

如果我们不打算从特定的节点上取出指定数量的哈希槽， 那么可以向 redis-trib 输入 all ， 这样的话， 集群中的所有主节点都会成为源节点， redis-trib 将从各个源节点中各取出一部分哈希槽， 凑够 10 个， 然后移动到目标节点上面

```shell script
...
Please enter all the source node IDs.
Type 'all' to use all the nodes as source nodes for the hash slots.
Type 'done' once you entered all the source nodes IDs.
Source node #1:all
```

输入 all 并按下回车之后， redis-trib 将打印出哈希槽的移动计划， 如果你觉得没问题的话， 就可以输入 yes 并再次按下回车：

```shell script
Ready to move 10 slots.
  Source nodes:
    M: 846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000
   slots:0-5460 (5461 slots) master
   1 additional replica(s)
    M: 878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002
   slots:10923-16383 (5461 slots) master
   1 additional replica(s)
  Destination node:
    M: f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001
   slots:5461-10922 (5462 slots) master
   1 additional replica(s)
  Resharding plan:
    Moving slot 0 from 846afdad3b4cf5e1339113eadcd76c9976354b8a
    Moving slot 1 from 846afdad3b4cf5e1339113eadcd76c9976354b8a
    Moving slot 2 from 846afdad3b4cf5e1339113eadcd76c9976354b8a
    Moving slot 3 from 846afdad3b4cf5e1339113eadcd76c9976354b8a
    Moving slot 4 from 846afdad3b4cf5e1339113eadcd76c9976354b8a
    Moving slot 10923 from 878176fcd293a3fb66a4b67b54f104503e6041ba
    Moving slot 10924 from 878176fcd293a3fb66a4b67b54f104503e6041ba
    Moving slot 10925 from 878176fcd293a3fb66a4b67b54f104503e6041ba
    Moving slot 10926 from 878176fcd293a3fb66a4b67b54f104503e6041ba
    Moving slot 10927 from 878176fcd293a3fb66a4b67b54f104503e6041ba
Do you want to proceed with the proposed reshard plan (yes/no)? yes 
```

输入 yes 并使用按下回车之后， redis-trib 就会正式开始执行重新分片操作， 将指定的哈希槽从源节点一个个地移动到目标节点上面：

```shell script
Moving slot 0 from 8.135.52.239:7000 to 8.135.52.239:7001@17001: 
Moving slot 1 from 8.135.52.239:7000 to 8.135.52.239:7001@17001: 
Moving slot 2 from 8.135.52.239:7000 to 8.135.52.239:7001@17001: 
Moving slot 3 from 8.135.52.239:7000 to 8.135.52.239:7001@17001: 
Moving slot 4 from 8.135.52.239:7000 to 8.135.52.239:7001@17001: 
Moving slot 10923 from 8.135.52.239:7002@17002 to 8.135.52.239:7001@17001: 
Moving slot 10924 from 8.135.52.239:7002@17002 to 8.135.52.239:7001@17001: 
Moving slot 10925 from 8.135.52.239:7002@17002 to 8.135.52.239:7001@17001: 
Moving slot 10926 from 8.135.52.239:7002@17002 to 8.135.52.239:7001@17001: 
Moving slot 10927 from 8.135.52.239:7002@17002 to 8.135.52.239:7001@17001: 
```

在重新分片操作执行完毕之后， 可以使用以下命令来检查集群是否正常：

```shell script

docker run -it --net host inem0o/redis-trib check 8.135.52.239:7000

>>> Performing Cluster Check (using node 8.135.52.239:7000)
M: 846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000
   slots:5-5460 (5456 slots) master
   1 additional replica(s)
S: 5d3bf967e589615df6863e0dc8dd154ee83311d3 8.135.52.239:7005@17005
   slots: (0 slots) slave
   replicates 878176fcd293a3fb66a4b67b54f104503e6041ba
S: 1077e8df06eef691432edd01d677f9f26018ebf5 8.135.52.239:7004@17004
   slots: (0 slots) slave
   replicates f5852f7fd2e4f4e93803637bb8ff57408a30f488
M: f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001
   slots:0-4,5461-10927 (5472 slots) master
   1 additional replica(s)
M: 878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002
   slots:10928-16383 (5456 slots) master
   1 additional replica(s)
S: 1f90f143f6ed56ed315bfec49b5af61a092f0c03 8.135.52.239:7003@17003
   slots: (0 slots) slave
   replicates 846afdad3b4cf5e1339113eadcd76c9976354b8a
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
```

根据检查结果显示， 集群运作正常。

需要注意的就是， 在三个主节点中， 节点 8.135.52.239:7001 包含了 5472 个哈希槽， 而节点 8.135.52.239:7000 和节点 8.135.52.239:7002 都只包含了 5456 个哈希槽， 因为后两者都将自己的 5 个哈希槽移动到了节点 8.135.52.239:7001 。


## 故障转移测试
:::tip
请继续执行php程序
:::

要触发一次故障转移， 最简单的办法就是令集群中的某个主节点进入下线状态。

首先用以下命令列出集群中的所有主节点：

```shell script
/data # redis-cli -p 7000 cluster nodes | grep master
f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001 master - 0 1613283240000 7 connected 0-4 5461-10927
846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000@17000 myself,master - 0 1613283241000 1 connected 5-5460
878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002 master - 0 1613283242000 3 connected 10928-16383
```

通过命令输出， 我们知道端口号为 7000 、 7001 和 7002 的节点都是主节点， 然后我们可以通过向端口号为 7002 的主节点发送 DEBUG SEGFAULT 命令， 让这个主节点崩溃：

```shell script
$ redis-cli -p 7002 debug segfault
Error: Server closed the connection

# docker环境需要停止容器
docker stop 容器ID
docker start 容器ID
```

现在， 让我们使用 cluster nodes 命令， 查看集群在执行故障转移操作之后， 主从节点的布局情况

```shell script
/data # redis-cli -p 7000 cluster nodes
1f90f143f6ed56ed315bfec49b5af61a092f0c03 8.135.52.239:7003@17003 slave 846afdad3b4cf5e1339113eadcd76c9976354b8a 0 1613284104032 1 connected
846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000@17000 myself,master - 0 1613284105000 1 connected 5-5460
878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002 master - 0 1613284105000 3 connected 10928-16383
f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001 slave 1077e8df06eef691432edd01d677f9f26018ebf5 0 1613284105000 8 connected
1077e8df06eef691432edd01d677f9f26018ebf5 8.135.52.239:7004@17004 master - 0 1613284105335 8 connected 0-4 5461-10927
5d3bf967e589615df6863e0dc8dd154ee83311d3 8.135.52.239:7005@17005 slave 878176fcd293a3fb66a4b67b54f104503e6041ba 0 1613284105535 3 connecte
```

我重启了之前下线的 8.135.52.239:7001 节点， 该节点已经从原来的主节点变成了从节点， 而现在集群中的三个主节点分别是 8.135.52.239:7000 、 8.135.52.239:7002 和 8.135.52.239:7004 ， 其中 8.135.52.239:7004 就是因为 8.135.52.239:7001 下线而变成主节点的。

## 添加新节点到集群(add-node)

根据新添加节点的种类， 我们需要用两种方法来将新节点添加到集群里面：

* 如果要添加的新节点是一个主节点， 那么我们需要创建一个空节点（empty node）， 然后将某些哈希桶移动到这个空节点里面。
* 另一方面， 如果要添加的新节点是一个从节点， 那么我们需要将这个新节点设置为集群中某个节点的复制品（replica）。

无论添加的是那种节点， 第一步要做的总是添加一个空节点。

我们可以继续使用之前启动 8.135.52.239:7000 、 8.135.52.239:7001 等节点的方法， 创建一个端口号为 7006 的新节点， 使用的配置文件也和之前一样， 只是记得要将配置中的端口号改为 7006 。

接下来， 执行以下命令， 将这个新节点添加到集群里面：
```shell script
#命令中的 add-node 表示我们要让 redis-trib 将一个节点添加到集群里面， add-node 之后跟着的是新节点的 IP 地址和端口号， 再之后跟着的是集群中任意一个已存在节点的 IP 地址和端口号， 这里我们使用的是 8.135.52.239:7000 。
docker run -it --net host inem0o/redis-trib add-node 8.135.52.239:7006 8.135.52.239:7000

#或者
./redis-trib.rb add-node 8.135.52.239:7006 8.135.52.239:7000
```

通过 cluster nodes 命令， 我们可以确认新节点 8.135.52.239:7006 已经被添加到集群里面了：

```shell script
redis-cli -p 7006 cluster nodes
1f90f143f6ed56ed315bfec49b5af61a092f0c03 8.135.52.239:7003@17003 slave 846afdad3b4cf5e1339113eadcd76c9976354b8a 0 1613285088053 1 connected
5d3bf967e589615df6863e0dc8dd154ee83311d3 8.135.52.239:7005@17005 slave 878176fcd293a3fb66a4b67b54f104503e6041ba 0 1613285088000 3 connected
f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001 slave 1077e8df06eef691432edd01d677f9f26018ebf5 0 1613285087553 8 connected
846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000@17000 master - 0 1613285087000 1 connected 5-5460
1077e8df06eef691432edd01d677f9f26018ebf5 8.135.52.239:7004@17004 master - 0 1613285088555 8 connected 0-4 5461-10927
8e31ddb2614b800cb3b93d415f17159f237334ba 8.135.52.239:7006@17006 myself,master - 0 1613285087000 0 connected
878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002 master - 0 1613285089056 3 connected 10928-16383
```

新节点现在已经连接上了集群， 成为集群的一份子， 并且可以对客户端的命令请求进行转向了， 但是和其他主节点相比， 新节点还有两点区别：

* 新节点没有包含任何数据， 因为它没有包含任何哈希桶。
* 尽管新节点没有包含任何哈希桶， 但它仍然是一个主节点。

接下来， 只要使用 redis-trib 程序， 将集群中的某些哈希桶移动到新节点里面， 新节点就会成为真正的主节点了。

因为使用 redis-trib 移动哈希桶的方法在前面已经操作过， 所以这里就不再重复操作了。

现在， 让我们来看看， 将一个新节点转变为某个主节点的复制品（也即是从节点）的方法。

```shell script
127.0.0.1:7006> cluster relicate 846afdad3b4cf5e1339113eadcd76c9976354b8a
```

其中命令提供的 846afdad3b4cf5e1339113eadcd76c9976354b8a 就是主节点 8.135.52.239:7005 的节点 ID 。

```shell script
redis-cli -p 7000 cluster nodes | grep 846afdad3b4cf5e1339113eadcd76c9976354b8a
8e31ddb2614b800cb3b93d415f17159f237334ba 8.135.52.239:7006@17006 slave 846afdad3b4cf5e1339113eadcd76c9976354b8a 0 1613285648052 1 connected
1f90f143f6ed56ed315bfec49b5af61a092f0c03 8.135.52.239:7003@17003 slave 846afdad3b4cf5e1339113eadcd76c9976354b8a 0 1613285647552 1 connected
846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000@17000 myself,master - 0 1613285649000 1 connected 5-5460

```

846afdad3b4cf5e1339113eadcd76c9976354b8a 现在有两个从节点， 一个从节点的端口号为 7003 ， 而另一个从节点的端口号为 7006 。

## 删除集群节点(del-node)

```shell script
# del-node host:port node_id
docker run -it --net host inem0o/redis-trib del-node 8.135.52.239:7006 8e31ddb2614b800cb3b93d415f17159f237334ba

#查看集群
redis-cli -p 7000 cluster nodes
1f90f143f6ed56ed315bfec49b5af61a092f0c03 8.135.52.239:7003@17003 slave 846afdad3b4cf5e1339113eadcd76c9976354b8a 0 1613286054984 1 connected
846afdad3b4cf5e1339113eadcd76c9976354b8a 8.135.52.239:7000@17000 myself,master - 0 1613286055000 1 connected 5-5460
878176fcd293a3fb66a4b67b54f104503e6041ba 8.135.52.239:7002@17002 master - 0 1613286055485 3 connected 10928-16383
f5852f7fd2e4f4e93803637bb8ff57408a30f488 8.135.52.239:7001@17001 slave 1077e8df06eef691432edd01d677f9f26018ebf5 0 1613286054583 8 connected
1077e8df06eef691432edd01d677f9f26018ebf5 8.135.52.239:7004@17004 master - 0 1613286055000 8 connected 0-4 5461-10927
5d3bf967e589615df6863e0dc8dd154ee83311d3 8.135.52.239:7005@17005 slave 878176fcd293a3fb66a4b67b54f104503e6041ba 0 1613286055986 3 connected
```








