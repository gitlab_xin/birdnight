module.exports = {
    theme: 'reco',
    title: '路漫漫其修远兮～',
    description: 'Hello World',
    port: 9090,
    plugins: ['@vuepress/last-updated'],
    themeConfig: {
        // 博客配置
        blogConfig: {
            category: {
                location: 2,     // 在导航栏菜单中所占的位置，默认2
                text: 'Category' // 默认文案 “分类”
            },
            tag: {
                location: 3,     // 在导航栏菜单中所占的位置，默认3
                text: 'Tag',      // 默认文案 “标签”

            },
            socialLinks: [     // 信息栏展示社交信息
                { icon: 'reco-home', link: 'http://jenkins.birdnight.cn/' },
                { icon: 'reco-mail', link: '1064059707@qq.com' }
            ]
        },
        author: '鑫',
        record: '粤ICP备19162158号-1',
        recordLink: 'https://beian.miit.gov.cn/',
        sidebar: 'auto',
        type: 'blog',
        logo: '/img/logo.jpeg',
        authorAvatar: '/img/avatar.jpeg',
        nextLinks: true,
        prevLinks: true,
    }
}
