<?php
/**
 * Created by PhpStorm.
 * User: macxin
 * Date: 2021/2/13
 * Time: 6:59 下午
 */
namespace app\php;

use RedisCluster;

$host = [
  '8.135.52.239:7004'
];
$redisCluster = new RedisCluster(NULL, $host);

for ($x = 0; $x < 10000000; $x++){
    $k = 'user-1-k-'.$x;
    $v = 'user-1-v-'.$x;
    $redisCluster->set($k, $v);
    echo "SET {$k} {$v}".PHP_EOL;

    $k = 'user-2-k-'.$x;
    $v = 'user-2-v-'.$x;
    $redisCluster->set($k, $v);
    echo "SET {$k} {$v}".PHP_EOL;

    sleep(1);
}


